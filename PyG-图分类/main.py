import time
import torch
from torch_geometric.datasets import TUDataset
from torch_geometric.data import DataLoader
from torch.nn import Linear
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, GraphConv
from torch_geometric.nn import global_mean_pool

dataset = TUDataset('data/TUDataset', name='MUTAG')

# print(dataset)
# print(len(dataset))
# print(dataset.num_features)
# print(dataset.num_classes)
#
# data = dataset[0]
# print(data)
# print('======================================')
#
# print(data.num_nodes)
# print(data.num_edges)
#
# print('Average node degree:%.2f' % (data.num_edges / data.num_nodes))
# # print('Has isolated nodes:', data.has_isolated_nodes())
# # print(data.has_self_loops())
# print(data.is_undirected())

torch.manual_seed(12345)
dataset = dataset.shuffle()

train_dataset = dataset[:150]
test_dataset = dataset[150:]

train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)
test_loader = DataLoader(test_dataset, batch_size=64, shuffle=False)


class GCN(torch.nn.Module):
    def __init__(self, hidden_channels):
        super(GCN, self).__init__()
        torch.manual_seed(12345)
        self.conv1 = GCNConv(dataset.num_node_features, hidden_channels)
        self.conv2 = GCNConv(hidden_channels, hidden_channels)
        self.conv3 = GCNConv(hidden_channels, hidden_channels)
        self.lin = Linear(hidden_channels, dataset.num_classes)

    def forward(self, x, edge_index, batch):
        # 1.获得节点嵌入
        x = self.conv1(x, edge_index)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = x.relu()
        x = self.conv3(x, edge_index)

        # 2.Readout layer
        x = global_mean_pool(x, batch)  # [batch_size, hidden_channel]

        # 3.分类器
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin(x)

        return x

# 省略了邻域归一化，并为 GNN 层添加了一个简单的跳跃连接，以保留中心节点信息
# 简单来说，使用GraphConv类可以提升分类准确率
class GNN(torch.nn.Module):
    def __init__(self, hidden_channels):
        super(GNN, self).__init__()
        torch.manual_seed(12345)
        self.conv1 = GraphConv(dataset.num_node_features, hidden_channels)
        self.conv2 = GraphConv(hidden_channels, hidden_channels)
        self.conv3 = GraphConv(hidden_channels, hidden_channels)
        self.lin = Linear(hidden_channels, dataset.num_classes)

    def forward(self, x, edge_index, batch):
        # 1.获得节点嵌入
        x = self.conv1(x, edge_index)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = x.relu()
        x = self.conv3(x, edge_index)

        # 2.Readout layer
        x = global_mean_pool(x, batch)  # [batch_size, hidden_channel]

        # 3.分类器
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin(x)

        return x


# model = GCN(hidden_channels=64)
model = GNN(hidden_channels=64)
# 模型放入GPU
model = model.cuda()
print(model)

optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
criterion = torch.nn.CrossEntropyLoss()


def train():
    model.train()
    for data in train_loader:
        # 数据放入GPU
        data.x, data.y, data.edge_index, data.batch = data.x.cuda(), data.y.cuda(), data.edge_index.cuda(), data.batch.cuda()
        optimizer.zero_grad()
        out = model(data.x, data.edge_index, data.batch)
        loss = criterion(out, data.y)
        loss.backward()
        optimizer.step()


def test(loader):
    model.eval()
    correct = 0
    for data in loader:
        data.x, data.y, data.edge_index, data.batch = data.x.cuda(), data.y.cuda(), data.edge_index.cuda(), data.batch.cuda()
        out = model(data.x, data.edge_index, data.batch)
        pred = out.argmax(dim=1)
        correct += int((pred == data.y).sum())
    return correct / len(loader.dataset)


begin = time.time()

for epoch in range(200):
    print(epoch)
    train()
    train_acc = test(train_loader)
    test_acc = test(test_loader)
    print('Epoch:%03d, Train acc: %.4f, Test acc: %.4f' % (epoch, train_acc, test_acc))

end = time.time()
run_time = end - begin
print(run_time)
