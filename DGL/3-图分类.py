import torch
import torch.nn as nn
import torch.nn.functional as F
import dgl.data

# Generate a synthetic dataset with 10000 graphs, ranging from 10 to 500 nodes.
dataset = dgl.data.GINDataset('PROTEINS', self_loop=True)
print('Node feature dimensionality:', dataset.dim_nfeats)
print('Number of graph categories:', dataset.gclasses)

'''
图分类数据集包含两种类型元素：一组图及对应的图标签。
和图像分类任务相似，当数据集足够大时，需要使用小批量进行训练。
一般在图像分类时是使用DataLoader来小批量迭代数据集，在DGL中，使用GraphDataLoader。
并且使用SubsetRandomSampler从数据集中抽取其中的部分数据。
'''

from dgl.dataloading import GraphDataLoader  # 小批量迭代数据
from torch.utils.data.sampler import SubsetRandomSampler  # 从数据集中抽取部分数据

num_examples = len(dataset)
num_train = int(num_examples * 0.8)

train_sampler = SubsetRandomSampler(torch.arange(num_train))
test_sampler = SubsetRandomSampler(torch.arange(num_train, num_examples))

train_dataloader = GraphDataLoader(
    dataset=dataset, sampler=train_sampler, batch_size=5, drop_last=False
)
test_dataloader = GraphDataLoader(
    dataset=dataset, sampler=test_sampler, batch_size=5, drop_last=False
)

it = iter(train_dataloader)
batch = next(it)
print(batch)

batched_graph, labels = batch  # 返回批处理的图以及类别
print('Number of nodes for each graph element in the batch:', batched_graph.batch_num_nodes())
print('Number of edges for each graph element in the batch:', batched_graph.batch_num_edges())

# Recover the original graph elements from the mini-batch
graphs = dgl.unbatch(batched_graph)
print('The original graphs in the mini-batch')
print(graphs)

'''
图级分类和节点分类的区别
1、由于图分类任务是预测整个图的单个类别，而不是对每个节点进行预测，
因此在图分类任务重需要聚合所有节点和边的表征，以此形成一个图级别的表示，这个过程被称为Readout，
这里使用一个较为简单的Readout方式：dgl.mean_nodes()
2、模型的输入图使用GraphDataLoader生成的批处理图，
DGL提供的Readout函数是可以处理批处理图，将会为每个批处理元素返回一种表征。
'''

from dgl.nn.pytorch import GraphConv


class GCN(nn.Module):
    def __init__(self, in_feats, h_feats, num_classes):
        super(GCN, self).__init__()
        self.conv1 = GraphConv(in_feats, h_feats)
        self.conv2 = GraphConv(h_feats, num_classes)

    def forward(self, g, in_feat):
        h = self.conv1(g, in_feat)
        h = F.relu(h)
        h = self.conv2(g, h)
        g.ndata['h'] = h
        return dgl.mean_nodes(g, 'h')


# Create the model with given dimension
model = GCN(dataset.dim_nfeats, 16, dataset.gclasses)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

for epoch in range(20):
    for b_idx, (batched_graph, labels) in enumerate(train_dataloader):
        pred = model(batched_graph, batched_graph.ndata['attr'].float())
        loss = F.cross_entropy(pred, labels)
        print('epoch:', epoch, 'b_idx:', b_idx, 'loss:', loss.item())
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

num_correct = 0
num_tests = 0
for batched_graph, labels in test_dataloader:
    pred = model(batched_graph, batched_graph.ndata['attr'].float())
    num_correct += (pred.argmax(1) == labels).sum().item()
    num_tests += len(labels)

print('Test accuracy:', num_correct / num_tests)
