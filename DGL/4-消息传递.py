'''
图神经网络的计算模式大致相似，节点的Embedding需要汇聚其邻接节点Embedding以更新，
从线性代数的角度来看，这就是邻接矩阵和特征矩阵相乘。然而邻接矩阵通常都会很大，
因此另一种计算方法是将邻居的Embedding传递到当前节点上，再进行更新。
很多图并行框架都采用消息传递的机制进行运算(比如Google的Pregel)。而图神经网络框架DGL也采用了这样的思路。

DGL 的核心为消息传递机制（message passing），
主要分为消息函数 （message function）和汇聚函数（reduce function）。
1. 消息函数（message function）：
传递消息的目的是将节点计算时需要的信息传递给它，
因此对每条边来说，每个源节点将会将自身的Embedding（e.src.data）和边的Embedding(edge.data)传递到目的节点；
对于每个目的节点来说，它可能会受到多个源节点传过来的消息，它会将这些消息存储在”邮箱”中。
2. 汇聚函数（reduce function）：
汇聚函数的目的是根据邻居传过来的消息更新跟新自身节点Embedding，
对每个节点来说，它先从邮箱中汇聚消息函数所传递过来的消息（message），并清空邮箱）内消息；
然后该节点结合汇聚后的结果和该节点原Embedding，更新节点Embedding。
下面我们以GCN的算法为例，详细说明消息传递的机制是如何work的。
'''

# Step1 引入相关包
import dgl
import dgl.function as fn
import torch
import torch.nn as nn
import torch.nn.functional as F
from dgl import DGLGraph

# Step2 定义GCN的message函数和reduce函数
gcn_msg = fn.copy_src(src='h', out='m')
gcn_reduce = fn.sum(msg='m', out='h')


# Step3 定义一个应用与节点的node UDF(user defined function)
class NodeApplyModule(nn.Module):
    def __init__(self, in_feats, out_feats, activation):
        super(NodeApplyModule, self).__init__()
        self.linear = nn.Linear(in_feats, out_feats)
        self.activation = activation

    def forward(self, node):
        h = self.linear(node.data['h'])
        h = self.activation(h)
        return {'h': h}


# Step4 定义GCN的Embedding更新层
class GCN(nn.Module):
    def __init__(self, in_feats, out_feats, activation):
        super(GCN, self).__init__()
        self.apply_mod = NodeApplyModule(in_feats, out_feats, activation)

    def forward(self, g, feature):
        g.ndata['h'] = feature
        g.update_all(gcn_msg, gcn_reduce)
        g.apply_nodes(func=self.apply_mod)
        return g.ndata.pop('h')


# Step5 定义一个包含两个GCN层的图神经网络分类器
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.gcn1 = GCN(1433, 16, F.relu)
        self.gcn2 = GCN(16, 7, F.relu)

    def forward(self, g, features):
        x = self.gcn1(g, features)
        x = self.gcn2(g, x)
        return x


net = Net()
print(net)

# Step6 加载Cora数据集，并进行数据预处理
from dgl.data import citation_graph as citegrh
import networkx as nx


def load_cora_data():
    data = citegrh.load_cora()
    features = torch.FloatTensor(data.features)
    labels = torch.LongTensor(data.labels)
    mask = torch.ByteTensor(data.train_mask)
    g = data.graph
    # 去自环
    g.remove_edges_from(nx.selfloop_edges(g))
    g = DGLGraph(g)
    g.add_edges(g.nodes(), g.nodes())
    return g, features, labels, mask


# Step7 训练GCN神经网络
import time
import numpy as np

g, features, labels, mask = load_cora_data()
optimizer = torch.optim.Adam(net.parameters(), lr=1e-3)
dur = []
for epoch in range(30):
    if epoch >= 3:
        t0 = time.time()

    logits = net(g, features)

    # 为什么不直接用交叉熵?
    # logp = F.log_softmax(logits, 1)
    # loss = F.nll_loss(logp[mask], lables[mask])
    # 可以用，等价，cross_entropy=log_softmax+nll_loss
    loss = F.cross_entropy(logits[mask], labels[mask])
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    if epoch >= 3:
        dur.append(time.time() - t0)
    print("Epoch {:05d} | Loss {:.4f} | Time(s) {:.4f}".format(epoch, loss.item(), np.mean(dur)))
