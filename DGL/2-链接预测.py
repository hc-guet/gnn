import dgl
import torch
import torch.nn as nn
import torch.nn.functional as F
import itertools
import numpy as np
import scipy.sparse as sp
import dgl.data
import dgl.function as fn
from sklearn.metrics import roc_auc_score

dataset = dgl.data.CoraGraphDataset('data/Cora')
g = dataset[0]

'''
正负数据划分：
随机抽取数据集中10%的边作为测试集中的正样例，剩下的90%数据集中的边作为训练集，
负采样生成相同数量的负样例，使得训练集和测试集中的正负样本比例为1:1，
将数据集中边的集合划分到训练集和测试集中，训练集90%，测试集10%
'''
u, v = g.edges()
eids = np.arange(g.number_of_edges())
eids = np.random.permutation(eids)  # 随机打乱顺序
test_size = int(len(eids) * 0.1)
train_size = g.number_of_edges() - test_size
test_pos_u, test_pos_v = u[eids[:test_size]], v[eids[:test_size]]  # 正采样测试集
train_pos_u, train_pos_v = u[eids[test_size:]], v[eids[test_size:]]  # 正采样训练集

# 采样所有负样例并划分为训练集和测试集中
adj = sp.coo_matrix((np.ones(len(u)), (u.numpy(), v.numpy())))  # (4, 1761)	1.0 存储连接的边
# adj.todense() 转换为邻接矩阵，shape=(2708, 2708)
adj_neg = 1 - adj.todense() - np.eye(g.number_of_nodes())  # 将加自环的邻接矩阵取反
neg_u, neg_v = np.where(adj_neg != 0)  # 得到非连接的边

neg_eids = np.random.choice(len(neg_u), g.number_of_edges() // 2)
test_neg_u, test_neg_v = neg_u[neg_eids[:test_size]], neg_v[neg_eids[:test_size]]  # 负采样测试集
train_neg_u, train_neg_v = neg_u[neg_eids[test_size:]], neg_v[neg_eids[test_size:]]  # 负采样训练集

'''在模型训练时，需要将图中在测试集中10%的边移除掉，防止数据泄露，使用dgl.remove_edges'''
train_g = dgl.remove_edges(g, eids[:test_size])

from dgl.nn.pytorch import SAGEConv


# 定义一个两层的GraphSage模型
class GraphSAGE(nn.Module):
    def __init__(self, in_feats, h_feats):
        super(GraphSAGE, self).__init__()
        self.conv1 = SAGEConv(in_feats, h_feats, 'mean')
        self.conv2 = SAGEConv(h_feats, h_feats, 'mean')

    def forward(self, g, in_feat):
        h = self.conv1(g, in_feat)
        h = F.relu(h)
        h = self.conv2(g, h)
        return h


'''
模型通过定义函数来预测两个节点表示之间的得分，从而来判断两个节点之间存在边的可能性，
在GNN节点分类任务中，模型是训练得到单个节点的表征，但在链接计算任务中是预测节点对的表征
DGL使用方式是先将节点对视为一个图，同时一条边可用来描述一对节点。
在链接预测中，会得到一个正图，它包含所有的正例子作为边，
以及一个负图，它包含所有的负例子。
正图和负图将包含与原始图相同的节点集。这使得在多个图中传递节点特征更容易进行计算。
可以直接将在整个图上计算的节点表示形式提供给正图和负图，用于计算节点对的两两得分。
'''
# 训练集正负采样
train_pos_g = dgl.graph((train_pos_u, train_pos_v), num_nodes=g.number_of_nodes())
train_neg_g = dgl.graph((train_neg_u, train_neg_v), num_nodes=g.number_of_nodes())
# 测试集正负采样
test_pos_g = dgl.graph((test_pos_u, test_pos_v), num_nodes=g.number_of_nodes())
test_neg_g = dgl.graph((test_neg_u, test_neg_v), num_nodes=g.number_of_nodes())


# 定义两个节点之间的得分函数预测,可以直接使用DGL提供的，也可以自定义，
# 下面DotPredictor是官方提供的预测函数，MLPPredictor是自定义的预测函数
class DotPredictor(nn.Module):
    def forward(self, g, h):
        with g.local_scope():
            g.ndata['h'] = h
            # 通过源节点特征和目标节点特征之间的点积计算两点之间存在边的Score/Possibility
            g.apply_edges(fn.u_dot_v('h', 'h', 'score'))
            # u_dot_v为每条边返回一个元素向量，因此需要squeeze操作
            return g.edata['score'][:, 0]


class MLPPredictor(nn.Module):
    def __init__(self, h_feats):
        super(MLPPredictor, self).__init__()
        self.W1 = nn.Linear(h_feats * 2, h_feats)
        self.W2 = nn.Linear(h_feats, 1)

    def apply_edges(self, edges):
        """
        Computes a scalar score for each edge of the given graph.

        Parameters
        ----------
        edges :
            Has three members ``src``, ``dst`` and ``data``, each of
            which is a dictionary representing the features of the
            source nodes, the destination nodes, and the edges
            themselves.

        Returns
        -------
        dict
            A dictionary of new edge features.
        """
        h = torch.cat([edges.src['h'], edges.dst['h']], 1)
        return {'score': self.W2(F.relu(self.W1(h))).squeeze(1)}

    def forward(self, g, h):
        with g.local_scope():
            g.ndata['h'] = h
            g.apply_edges(self.apply_edges)
            return g.edata['score']


model = GraphSAGE(train_g.ndata['feat'].shape[1], 16)
print(model)
pred = DotPredictor()   # 效果好一些
# pred = MLPPredictor(16)


def compute_loss(pos_score, neg_score):
    scores = torch.cat([pos_score, neg_score])
    labels = torch.cat([torch.ones(pos_score.shape[0]), torch.zeros(neg_score.shape[0])])
    return F.binary_cross_entropy_with_logits(scores, labels)


def compute_auc(pos_score, neg_score):
    scores = torch.cat([pos_score, neg_score]).numpy()
    labels = torch.cat(
        [torch.ones(pos_score.shape[0]), torch.zeros(neg_score.shape[0])]).numpy()
    return roc_auc_score(labels, scores)


optimizer = torch.optim.Adam(itertools.chain(model.parameters(), pred.parameters()), lr=0.01)

# 训练
for e in range(100):
    h = model(train_g, train_g.ndata['feat'])
    pos_score = pred(train_pos_g, h)
    neg_score = pred(train_neg_g, h)
    loss = compute_loss(pos_score, neg_score)

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    if e % 5 == 0:
        print('In epoch {}, loss: {}'.format(e, loss))

# 检测结果准确性
with torch.no_grad():
    pos_score = pred(test_pos_g, h)
    neg_score = pred(test_neg_g, h)
    print('AUC', compute_auc(pos_score, neg_score))
