import dgl
import torch
import torch.nn as nn
import torch.nn.functional as F
import dgl.data
from dgl.nn.pytorch import GraphConv

dataset = dgl.data.CoraGraphDataset('data/Cora')
print('Number of categories:', dataset.num_classes)

g = dataset[0]
print(g)
print('Node features:', g.ndata)
'''
DGL库中一个Graph是使用字典的形式存储了Node Features和Edge Features，
g.ndata使用字典结构存储了节点特征信息
g.edata使用字典结构存储了边特征信息
Node Features共包含以下五个方面：
1. train_mask: 指示节点是否在训练集中的布尔张量
2. val_mask: 指示节点是否在验证集中的布尔张量
3. test_mask: 指示节点是否在测试机中的布尔张量
4. label: 每个节点的真实类别
5. feat: 节点自身的属性
'''
print('Edge features:', g.edata)


class GCN(nn.Module):
    def __init__(self, in_feats, h_feats, num_classes):
        super(GCN, self).__init__()
        self.conv1 = GraphConv(in_feats, h_feats)
        self.conv2 = GraphConv(h_feats, num_classes)

    def forward(self, g, in_feat):
        # 这里的g代表的Cora数据Graph信息，一般就是经过归一化的邻接矩阵
        # in_feat表示的是node representation, 即节点初始化特征信息
        h = self.conv1(g, in_feat)
        h = F.relu(h)
        h = F.relu(h)
        h = self.conv2(g, h)
        return h


# 使用给定的维度创建GCN模型，其中hidden维度设定为16，输入维度和输出维度由数据集确定。
model = GCN(g.ndata['feat'].shape[1], 16, dataset.num_classes)


def train(g, model):
    optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
    best_val_acc = 0
    best_test_acc = 0
    features = g.ndata['feat']
    labels = g.ndata['label']
    train_mask = g.ndata['train_mask']
    val_mask = g.ndata['val_mask']
    test_mask = g.ndata['test_mask']
    for e in range(100):
        # Forward
        logits = model(g, features)

        pred = logits.argmax(1)

        loss = F.cross_entropy(logits[train_mask], labels[train_mask])

        train_acc = (pred[train_mask] == labels[train_mask]).float().mean()
        val_acc = (pred[val_mask] == labels[val_mask]).float().mean()
        test_acc = (pred[test_mask] == labels[test_mask]).float().mean()

        # Sava the best validation accuracy and the corresponding test accuracy
        if best_val_acc < val_acc:
            best_val_acc = val_acc
            best_test_acc = test_acc

        # Backward
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if e % 5 == 0:
            print('In epoch {}, loss: {:.3f}, val acc: {:.3f} (best {:.3f}), test acc: {:.3f} (best {:.3f})'.format(
                e, loss, val_acc, best_val_acc, test_acc, best_test_acc))


# g, model = g.to('cuda'), model.to('cuda')   # 要使用torch版本的dgl
train(g, model)
