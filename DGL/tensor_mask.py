import torch
a = [[1, 3], [2, 4], [6, 5]]  # tensor([[1., 3.],[6., 5.]])
# a = [8, 2, 3]  # tensor([8., 3.])
a = torch.Tensor(a)
mask = torch.BoolTensor([True, False, True])
print(a[mask])

import numpy as np
print(np.eye(5))